//     *
//    **
//   ***
//  ****
// *****

 // left triangle program

class tri{
	public static void main(String []args){
		int row=5;
		for(int i=0;i<=row;i++){
			for(int j=1;j<=row-i+1;j++){
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++){
				System.out.print("*");
			}
			System.out.println();
		}
	}
}
	       
