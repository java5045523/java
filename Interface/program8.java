//In interface Static method is not inherited to the child class
interface Demo{
	static void fun(){
		   System.out.println("In Fun Demo1");
	}
}
class DemoChild implements Demo{
	}
class Client{
	public static void main(String []args){
		DemoChild obj=new DemoChild();
		Demo.fun();
		obj.fun();    //Error : cannot find symbol
	}
}
