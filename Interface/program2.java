interface Demo2{
	void fun();
	void gun();
}
 abstract class Demochild1 implements Demo2{
	 public void fun(){
		 System.out.println("In Fun method");
	 }
 }
class DemoChild extends Demochild1{
	public void gun(){
       System.out.println("In gun method");
	}
}
class Clientt{
	public static void main(String []args){
		Demo2 obj=new DemoChild();
		obj.fun();
		obj.gun();
	}
}
