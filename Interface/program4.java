//CODE 4: Interface . in 1.8 java update - we can include body to method in interface

interface Demoo1{
 static void m1(){
		System.out.println("Demo 1 -m1");
	}
} 
interface Demoo2{
 static	 void m1(){
		System.out.println("Demo 2- m1");
	}
}
interface Demoo3 extends Demoo1,Demoo2{

}
class DemooChild implements Demoo3{
	public static void main(String []args){
		DemooChild obj=new DemooChild();
		Demoo2.m1();
		
		//Demoo1.m1();
		//Demoo2.m1();
		//Demoo3.m1();

	}
}


