//Interface - 100% abstraction //all methods are abstract // Interface class has no Constructor // Methods are bydefault public
//Code1:

interface Demo{
	void fun();        //public abstract void fun 
        
	void gun();        //public abstract void gun

}

class DemoChild implements Demo{ 
	public void fun(){                      //mention public whenever overriding from interface
		System.out.println("In Fun");
	}
       public	 void gun(){
		System.out.println("In gun");
	}
         

}
class Client{
	public static void main(String []args){
		Demo obj=new DemoChild();
		obj.fun();
		obj.gun();
	}
}
