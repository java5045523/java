//Interface-- Use of default keyword in interface ,
interface Demo1{
	default void fun(){
		System.out.println("In Fun Demo1");
	}
}
interface Demo2{
	default void fun(){
		   System.out.println("In fun Demo2");
	}
}
class DemoChild implements Demo1,Demo2{
public void fun(){                                      //Overrided Fun method 
	   System.out.println("In Fun DemoCHild");
}
}


class Client1{
	public static void main(String []args){
		Demo1 obj=new DemoChild();
		obj.fun();  

	       Demo2 obj2=new DemoChild();
                obj2.fun();

                DemoChild obj3=new DemoChild();
                 obj3.fun();		
	}
}
//O/p:  In Fun demochild
//      In fun demochild
//      In fun demochild
