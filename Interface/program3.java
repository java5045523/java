//Multiple inheritance solved by java by Interface

interface demo1{
	void run();
}
interface demo2{
	void run();
}
class demoChild implements demo1,demo2{
	public void run(){
		System.out.println("In Run Method");
	}
}
class Client3{
	public static void main(String []args){
		demoChild obj1=new demoChild();
		obj1.run();

		demo obj2=new demoChild();
		obj2.run();
	}
}
