interface Demo{
	void gun();                             //public abstract  void gun
	 default void fun(){                    // public default void fun
			System.out.println("In Fun Demo");
		}
	}
class demochild implements Demo{
	public void gun(){
		   System.out.println("In Gun -Demochild");
	}
	public void fun(){                                       //fun method overrided 
		   System.out.println("In fun - demochild");
	}
}
class Client {
	public static void main(String []args){
		demochild obj=new demochild();
		obj.fun();      //In fun demo
		obj.gun();       //In gun Demochild
	}
}
//default method in interface is inherited in child class . but static method doesnot.
