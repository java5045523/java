//Variables in interface Code 2 

interface Demo{
       int x=10;                    //public static final int x 
	void fun();                    //public abstract void fun
}
class demochild implements Demo{
	public void fun(){
		   System.out.println(x);
		   System.out.println(Demo.x);
	}
}
class Client{
	public static void main(String []args){
		Demo obj=new demochild();
		obj.fun();
	}
}
//o/p: 10
//     10
