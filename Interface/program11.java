interface Demo1{
	static void fun(){
		System.out.println("In Fun Demo1");
	}
}
interface Demo2{
	static void fun(){
		   System.out.println("In Fun Demo2");
	}
}
class Demochild implements Demo1,Demo2{
	void fun(){
		   System.out.println("In Fun -CHILD");
	}
}
class Client{
	public static void main(String []args){
               Demo1.fun();
	       Demo2.fun();
		Demochild obj=new Demochild();
		obj.fun();
	}
}
