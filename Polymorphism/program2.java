// polymorphism - Overloading code.
class Poly2{
	void fun(int x){
		System.out.println(x);
	}
	void fun(float y){
		   System.out.println(y);
	}
	void fun(Poly2 gola){
		   System.out.println("In Poly2 parameter");
		      System.out.println(gola);
	}
public static void main(String []args){
	Poly2 obj=new Poly2();
	obj.fun(100);
	obj.fun(200.4f);
	Poly2 obj2=new Poly2();
	obj.fun(obj2);
}
}
