//OverRiding 2
class Dad{
	Dad(){
		  System.out.println("In Parent constructor");
	}
	void fun(){
		  System.out.println("In fun");
	}
}
class Son extends Dad{
	Son(){
		  System.out.println("In Child COnstructor");
	}
	void gun(){
		  System.out.println("In Gun");
	}
}
class User{
	public static void main(String []args){
		Son obj=new Son();
		obj.fun();
		obj.gun();

		Dad obj2=new Dad();
		obj2.fun();
	         //obj2.gun();             //Parent cannot access child method .//Error - Cannot find Symbol	

	}
}
