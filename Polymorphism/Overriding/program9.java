class Parent6{
	static void fun(){
		 System.out.println("In parent Fun");
	}
}
class Child6 extends Parent6{
	static void fun(){
		 System.out.println("In child fun");
	}
}
class Demo6Client{
	public static void main(String []args){
		Parent6 obj=new Parent6();
		obj.fun();

		Child6 obj2=new Child6();
		obj2.fun();

		Parent6 obj3=new Child6();
		obj3.fun();
	}
}
