//Acces specifier In OverRiding
class Parent3{
	private void fun(){
		 System.out.println("parent Fun");
	}
}
class Child3 extends Parent3{
	 void fun(){
		 System.out.println("Child Fun");
	}
}
//O/p : there is no overriding concept bcz parent fun method is private and doesnot inherit to child class
