//Access Specifier in OverRiding
class Parent2{
	public void fun(){                                        //Acces Specifier : public
		 System.out.println("Parent Fun Method");
	}
}
class Child2 extends Parent2{
      	void fun(){                                         //Access Specifier : defualt
		 System.out.println("Child Fun Method");
	}
}
//O/P :Error: Attempting to assign weaker access privileges , was public
//public to default . Scope reducing of method fun .

