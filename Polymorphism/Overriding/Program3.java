//IMP CODE
class Demo3{
	void fun(Object obj){
		 System.out.println("Object paramter -FUn method");
	}
	void fun(String str){
		 System.out.println("String Paramter FUN method");
	}
}
class Demo3Client{
	public static void main(String []args){
		Demo3 obj=new Demo3();
		obj.fun("Core2web");           //String Paramter 
		obj.fun(obj);                   //Object paramter
		obj.fun(new StringBuffer ("Core2web"));    // CO-variant relation parent child 
obj.fun(null);                                            //String paramter
	}
}
