class Father{
	Father(){
		    System.out.println("In parent constructor");
	}
	void fun(){
		    System.out.println("In parent Fun");
	}
//class Boy extends Father{
	Boy(){
		   System.out.println("In Child Constructor");
	}
	void fun(int s){
		   System.out.println("In child fun");
	}

//class user{
	public static void main(String []args){
	Father obj1 = new Boy();          //parents reference and object of child 
	obj1.fun(10);                       
	}
}
}

