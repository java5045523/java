class Parent1{
	Object fun(){
		 Object obj=new Object();
		  System.out.println("Parent Fun method");
	return obj;
	}
}
class Child1 extends Parent1{
	String fun(){
		 System.out.println("CHILD Fun method");
return "Karan";
	}
}
class Demo5Client{
	public static void main(String []args){
		Parent1 obj1=new Child1();
		obj1.fun();
	}
}
//O/p: Child Fun  Method
//co-variant relationship between Object _string (eg. parent and child)
