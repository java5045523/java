//IMP CODE
class Demo1{
	void fun(String str1){
		 System.out.println("String");
	}
	void fun(StringBuffer str1){
		 System.out.println("StringBUffer");
	}
}
class Demo1Client{
	public static void main(String []args){
		Demo1 obj=new Demo1();
		obj.fun("Core2Web");
		obj.fun(new StringBuffer("Core2Web")); //to call StringBUffer method we need to make obj of StringBUffer String
                 // obj.fun(null); // ambiguity error null can go to both methods compiler is confused
	}
	}

		
