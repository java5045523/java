// polymorphism- Overriding - two different class with parent child relation - 
class Parent{
	Parent(){
		System.out.println(" In Parent constructor");
	}
	void Property(){
		  System.out.println("Home ,Car, Gold");
	}
	void marry(){
		  System.out.println("Deepika paduKone");
	}
}
class Child extends Parent{
	Child(){
		  System.out.println("In child Constructor");
	}
	void marry(){                                               //Over rided Method
		  System.out.println("Alia Bhatt");
	}
}
class Client{
	public static void main(String []args){
		Child obj= new Child();
		obj.Property();
		obj.marry();             //Alia Bhatt
	}
}
