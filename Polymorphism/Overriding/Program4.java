class Dad{
 	char fun(){                                        //return typr - character
		 System.out.println("Parent FUn");
	return 'A';
	}
}
class Son extends Dad{
	int fun(){
		 System.out.println("Child fun");          //return type - int
	return 10;
	}
}
class Demo4client{
	public static void main(String []args){
		Dad D=new Son();
		D.fun();
	}
}

//Error: Fun method cannot be overRide return type is not same 
//primitive data type are strictly checked 
