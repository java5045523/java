//Acces modifier -Final Modifier in Overriding
class Parent5{
	final void fun(){
		 System.out.println("Parent Fun method");
	}
}
class Child5 extends Parent5{
	void fun(){
		 System.out.println("Child fun method");
	}
}
//O/P : error : Parent fun is final and cannot be overridden by the child method .
//overridden method is final /// Fun method cannot be inherit
