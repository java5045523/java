class P{
	P(){
		System.out.println("In Parent constructor");
		}
	void fun(){
		 System.out.println("In Parent Fun");
	}
}
class C extends P{
	C(){
		 System.out.println("In Child constructor");
	}
	void fun(){
		 System.out.println("In Child Fun");
	}
}
class Ct{
	public static void main(String []args){
		P obj =new C();
		obj.fun();
	}
}
