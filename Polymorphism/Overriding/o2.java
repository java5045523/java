class Pt{
	Pt(){
		 System.out.println("In Parent constructor");
	}
	void fun(int x){
		 System.out.println("In Parent fun");
	}
}
class Cd extends Pt{
	Cd(){
		 System.out.println("In Child constructor");
	}
	void fun(){
		 System.out.println("In Child Fun ");
		}
}
class Cl {
	public static void main(String []args){
		Cd obj=new Cd();
	obj.fun(10);
		
	}
}

