//Polymorphism - Same Class - Same method Name - different parameters - Overloading
class Demo{
	int x=20;
	int y=40;
	Demo(int a){
		  System.out.println(a);
	}

	void fun(int x){                         //same method int parameter  MS: fun (int)
	 System.out.println(x); 
	 }
	void fun(){
		System.out.println(x);           //same method null void parameter  fun()
	}
}
class Client{
	public static void main(String []args){
		Demo obj=new Demo(100);
		
		obj.fun(50);
		obj.fun();
	}
}

