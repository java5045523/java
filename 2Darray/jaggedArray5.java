// Jagged Array : Accept data from users and PRint .
import java.io.*;
class JaggedArrayDemo5{
	public static void main(String []args)throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader (System.in));

		int arr5[][]=new int[4][];
		arr5[0]= new int [3];
		arr5[1]=new int [2];
	       	arr5[2]=new int [1];
		arr5[3]=new int [5];
                 
		 System.out.println("Enter the values of array:");
		for (int i =0; i<arr5.length;i++){        //for row
		for (int j=0; j<arr5[i].length;j++){      // for arr5[0] [1] [2] . length  : COLUMNS
                  arr5[i][j]=Integer.parseInt(br.readLine());
		}
		}

		 for (int []x: arr5){
                        for (int y : x){
         System.out.print(y + " ");
                        }
			   System.out.println();
                }
	}
}


