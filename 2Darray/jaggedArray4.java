// initialization of jagged array in two ways.
class JaggedArrayDemo4{
        public static void main(String []args){
		int arr4[][]={{1,2,3},{2,4},{9}};          //1st Way
             
		int arr[][]=new int [3][];              //2 nd way to declare
		arr[0]=new int[]{1,2,3};
		   arr[1]=new int[]{2,3};
		      arr[2]=new int[]{3};

		      for (int x[] : arr){
			      for (int y :x ){
				      System.out.print(y + " ");
			      }
			      System.out.println();
		      }

		          for (int x[] : arr4){
                              for (int y :x ){
                                      System.out.print(y + " ");
                              }
                              System.out.println();
                      }
	}
}

