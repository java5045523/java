// Jagged array printing using for each loop.
class JaggedArrayDemo3{
	public static void main(String []args){
		int arr3[][]={{20,30,40,50},{40,24,4},{2,4}};

		for (int [] x: arr3){              //arr3[0] = {20 ,30 ,40 50} 
			for(int y :x){                         //int y= 20 ,30 ,40 ,50
				System.out.print(y + " " );
			}
			 System.out.println();
		}
	}
}
