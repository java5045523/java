// jagged array printing using for loop and using for each loop.
class JaggedArray2{
	public static void main(String []args){
		int arr2[][]={{10,20,30},{4,5},{7}};
		for(int i=0;i<arr2.length;i++){
			for(int j=0;j<arr2[i].length;j++){
				System.out.print(arr2[i][j]+ " ");

			}
			System.out.println();
		}
	}
}
