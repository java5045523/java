// wap to print product of odd index only.
import java.util.*;
class Assign3{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size");
		int size=sc.nextInt();
		int kar[]=new int[size];
		 System.out.println("Enter elements");
		 for(int i=0;i<kar.length;i++){
			 kar[i]=sc.nextInt();
		 }
                  int product=1;
		 for(int i=0;i<kar.length;i++){
			 if(i%2!=0){
				 product=product * kar[i];
			 }
		 }
		 System.out.println("Product of Odd Index elements:" + product);
	}
}
				 
