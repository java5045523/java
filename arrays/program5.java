// accept array of 5 size int from user and print the sum of elements
import java.io.*;
class ArrayDemo5{
	public static void main(String []args)throws IOException{
	
	BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
        System.out.println("Enter the array size");
	int size=Integer.parseInt(br.readLine());
	int arr[]=new int[size];
	System.out.println("Enter the array elements");
	
	for(int i=0;i<arr.length;i++){
		arr[i]=Integer.parseInt(br.readLine());
	}

int sum=0;
for(int i=0;i<5;i++){
	sum=sum+arr[i];
}
System.out.println("Sum of array elements is:" + sum);
}
}
