// Sequence:
class SDemo{

int x=10;
static int y=20;
SDemo(){
	System.out.println("Constructor");
}
static{
	System.out.println("Static Block1");
}

{
	   System.out.println("Instance Block1");
}
	   public static void main(String []args){
	 
		   SDemo obj=new SDemo();
}
{
	System.out.println("Instance  Block2");
}
static {
	   System.out.println("Static Block2");
}
}

//O/P: 
//    
//    Static block1
//    Static block2
//    Instance block1
//    Instance block2             
//    Constructor
//    In main
//     
