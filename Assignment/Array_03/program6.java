//WAP to print the palindrome no. from an array and return its index.
import java.io.*;
class ArrayDemo6{
	public static void main(String []args)throws IOException{
		BufferedReader br= new BufferedReader (new InputStreamReader (System.in));
		System.out.println("Enter the size");
		int size =Integer.parseInt(br.readLine());
		
		int arr[]= new int [size];
		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int rev=0;
			int temp=arr[i];
			while(temp!=0){
			int rem=temp%10;
			rev= rev*10 + rem;
			temp=temp/10;
			}

			if (arr[i]== rev){
				System.out.println(" palindrome no " + " "+ arr[i]+" " + "found at index " + i);
			}
		}
	}
}
