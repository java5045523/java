// NUllPOINTER Exception 
class NullPointerDemo{
	public static void main(String []args){
		int arr[][]={{},{},{}};       //initializer list complete array
	       
		int arr2[][]=new int [3][];

		System.out.println(arr.length);         //ans= 3 rows
		System.out.println(arr[0].length); 	// ans = 0 
             
		System.out.println(arr2.length);         //3
                System.out.println(arr2[0].length);      // Error : null pointer exception
		  
		
	}
}
