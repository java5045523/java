// wap to print reverse of each element from array.
import java.util.*;
class Arraydemo2{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array");
		int size=sc.nextInt();
		int arr[]= new int[size];
		   System.out.println("Enter the elements of array");
		   for (int i=0;i<arr.length;i++){
			   arr[i]=sc.nextInt();
		   }

		    for (int i=0;i<arr.length;i++){
			    int temp=arr[i];
			    int rev=0;
			    while(temp!=0){
				    int rem=temp%10;
				    rev=rev * 10 + rem;
			          temp=   temp/10;
			   
			    
			    }
			    System.out.println(rev); 
		    }
	}
}
