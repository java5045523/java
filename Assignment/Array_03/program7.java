// wap to print the armstrong from an array and return its index.
import java.io.*;
class ArrayDemo8{
        public static void main(String []args)throws IOException{
                BufferedReader br= new BufferedReader (new InputStreamReader (System.in));
                System.out.println("Enter the size");
                int size =Integer.parseInt(br.readLine());

                int arr[]= new int [size];
                System.out.println("Enter the elements");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		
		for(int i=0;i<arr.length;i++){
			int temp= arr[i];
int sum=0;
			while(temp!=0){
				int arm=0;
				int rem = temp%10;
				arm = rem * rem * rem;
				sum = sum + arm;
				temp=temp/10;
			}
			if (sum== arr[i]){
				System.out.println("Armstrong Number " + " " + arr[i] + " " + "Found at index : " + " " + i);
			}
		}
		}
	}


