class Demo extends Thread{
	public void run(){
		System.out.println("Demo:" + Thread.currentThread().getName());
	}
}
class Mythread extends Thread{
	public void run(){
		System.out.println("MyThread:" + Thread.currentThread().getName());
	}
}
class ThreadDemo{
	public static void main(String[] args){
		System.out.println("MainThread:" + Thread.currentThread().getName());
		Mythread obj=new Mythread();
		obj.start();
		Demo obj1=new Demo();
		obj1.start();
	}
}
