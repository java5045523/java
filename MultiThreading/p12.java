//Creating a threadgroup and assigning threads in it
class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
super(tg,str);
	}
	public void run(){

	        System.out.println(getName());
            System.out.println(getThreadGroup());
		System.out.println(Thread.currentThread());
	//System.out.println(Thread.currentThread().getThreadGroup());
System.out.println();
	}

}
class ThreadGroupDemo{
	public static void main(String[] args)throws InterruptedException{

		ThreadGroup pthreadgp=new ThreadGroup("Core2Web");

		MyThread obj1=new MyThread(pthreadgp,"JAVA");
		
		 MyThread obj2=new MyThread(pthreadgp,"C");
		 
		  MyThread obj3=new MyThread(pthreadgp,"C++");
		  obj1.start();
	obj1.join();
		  obj2.start();
		  obj2.join();
		  obj3.start();
		  obj3.join();
	}
}
