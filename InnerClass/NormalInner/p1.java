//Normal Inner Class
class Outer{
	class Inner{
		void fun2(){
			System.out.println("Fun2 - INNER");
		}
	}
	void fun1(){
		 System.out.println("Fun1 -OUTER");
	}
}
class Client1{
	 public static void main(String []args){
		 Outer obj=new Outer();
		 obj.fun1();

		 Outer.Inner obj1=obj.new Inner();
		 obj1.fun2();
	 }
}
