class Outer2{
        class Inner2{
                void fun2(){
                        System.out.println("Fun2 - INNER");
                System.out.println(this);
		}

        }
        void fun1(){
                 System.out.println("Fun1 -OUTER");
        }
}
class Client2{
	public static void main(String []args){
		Outer2 obj1=new Outer2();

		Outer2.Inner2 obj2=obj1.new Inner2();
		obj2.fun2();

		Outer2.Inner2 obj3=obj1.new Inner2();
	obj3.fun2();
	}
}

