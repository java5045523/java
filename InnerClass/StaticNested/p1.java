//Static nested Inner Class
class Outer{
  
	void m2(){
		   System.out.println("m1-Outer ");
	}
	static class Inner{

		void m1(){
			   System.out.println("m1-Inner ");
		}
	}
}

class Client{
	public static void main(String []args){
	Outer obj1=new Outer();
obj1.m2();	
		Outer.Inner obj=new Outer.Inner();
	obj.m1();
	
	
	}
}

