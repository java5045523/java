class Outer{
	class Inner{
		void m1(){
			System.out.println("Inner m1");
		}
	}
	void m2(){
		 System.out.println("Outer m2");
	}

public static void main(String []args){
	Inner obj=new Outer().new Inner();

	obj.m1();

	Outer.Inner obj1=new Outer().new Inner();
	obj1.m1();
	
}
}

