//Normal Inner Class Code
class Outer{
	class Inner{
		static void m1(){
			System.out.println("In m1-Inner Class");
}
	}	
	static void m2(){
		 System.out.println("In m2-Outer Class");
	}
}

class Client{
	public static void main(String []args){
		
	Outer obj=new Outer();
	obj.m2();


	Outer.Inner obj1=new Outer().new Inner(); //way 1st to create object of Inner class
	obj1.m1();

	Outer.Inner  obj2= obj.new Inner();      //way 2nd to create object of Inner class
	obj2.m1();
	}
}
