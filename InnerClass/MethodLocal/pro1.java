//MethodLocalInnerClass / Class is consisted into a another class method
class Outer{

	
	
		void m1(){
			System.out.println("M1 - Outer");
			class Inner{
				void m2(){
				System.out.println("M2-Inner1 ");
				class Inner2{
					void m2(){
						   System.out.println("M2-Inner2 ");
				}
			}
			Inner2 obj2=new Inner2();
			obj2.m2();
		}
			}
			Inner obj1=new Inner();
			obj1.m2();
		}

		void m3()
		{
			System.out.println("M3 - Outer");
		class Inner3{
		}
		}
			}

class Client{
	public static void main(String []args){
		Outer obj3=new Outer();
		obj3.m1();
		
		obj3.m3();
	}
}
