//++++++++++++++++++++++++++++++++++++++++Static Nested Inner Class+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class Outer {
	void m1(){
		 System.out.println("In M1 - OUTER CLASS");
		 }
	static class Inner{
		void m1(){
			 System.out.println("IN M1- INNER  CLASS");
		}
	}
}
class Client{
	public static void main(String []args){

		Outer.Inner obj=new Outer.Inner();
		obj.m1();
	}
}
