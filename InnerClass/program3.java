//Method Local Inner Class+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Inner class is written inside outer class method m1 and object is made of inner and called method of inner class.
class Outer{
	void m1(){
		 System.out.println("In m1- Outer");
	
class Inner{
	void m1(){
		 System.out.println("In m1-Inner");
	}
}
            Inner obj=new Inner();
	    obj.m1();
}
void m2(){
	 System.out.println("In m2 -outer");
}
public static void main(String []args){
	Outer obj=new Outer();
	obj.m1();
	obj.m2();
}
}

//O/p: In m1 - outer
//     In m1 - Inner
//     In m2- outer
