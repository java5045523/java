//Static variable method and block in inheritance .
class Papa{
         static int x=10;
        static{
                System.out.println("In parent Static block");
        }
        static void  fun(){
                 System.out.println("In Parent static method");

}
}

class Beta extends Papa{

        static {
                 System.out.println("In child Static block");
       
	
	}

	
}

class Mom{
        public static void main(String []args){
                Beta obj=new Beta();
 
 	}

}
//OP:     In parent s b
//        in child s b
//        in parent s m
                   
