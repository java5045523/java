//instance variable and method in inheritance concept

class parent{
	int x=10;
	parent(){
		System.out.println("In parent constructor");
	}
	void access(){
		System.out.println("Parent instance method");
	}
}
	class child extends parent{
		int y=20;
		child(){
			System.out.println("In child constructor");
			System.out.println(x);
			System.out.println(y);
		}
	}
	class Client{
		public static void main(String []args){
			child obj=new child();
			obj.access();
		}
	}

	//op:    in parent const
	//       in child const
	//       10 
	//       20

	//       parent instance method
	//       
