class Paarent{
	int x=10;
	static int y=20;
	static {
		  
        System.out.println("In parent static block");
	}
	Paarent(){
		      System.out.println("In parent Constructor");
	}
	void MethodOne(){
		      System.out.println(x);
		            System.out.println(y);
	}
	static void MethodTwo(){
		      System.out.println(y);
		
	}
}
class Chiild extends Paarent{
	static{
		      System.out.println("In Child Static Block");
	}
	Chiild(){
		      System.out.println(" In Child Constructor");
	}
}
class Cliient{
	public static void main(String []args){
		Chiild obj=new Chiild();
		obj.MethodOne();
		obj.MethodTwo();
	}
}
//OP:        In parent static block
//            In parent constructor
//            In child static block
//            in child constructor
//              10
//              20
//              20
