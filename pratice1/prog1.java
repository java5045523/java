// quiz 1 : break statement errors.
class quiz1{
	public static void main(String []args){
		int i=10;
		while(i>5){
			System.out.println(i);
			
			break;
			i--;      //unreachable statement
		}
	
}
}
