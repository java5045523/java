class quiz7{
	public static void main(String []args){
		int n=10;
		while(n>0){
			System.out.println(n); //loop will run from 10 9 8 7 6 5 .
			n--;                    //5 decrements to 4
			if(n<5)                    // 4<5 false
				break;
		}
	}
}
