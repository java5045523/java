//quiz 2 of break statement
class quiz2{
	public static void main(String []args){
		int n=10;
		int i=1;
		for( i=1;i<=n;i++){
			if(n==10)
				System.out.println("n = " + n);  //o/p is 10 and loop while occur 1 time only
			break;
		}
		System.out.println(i);
	}
}
