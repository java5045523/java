class Parent{
	int x=10;
	void m1(){
		System.out.println("In parent -m1");
	 
	}

}
class Child extends Parent{
	int a=20;
	void m1(){
		 System.out.println("In CHild - m1");
	}
}
class Demo{
	Demo(Parent P){
		 System.out.println("In Constructor - parent");
		 P.m1();
	}
	Demo(Child C){
		 System.out.println("In Constructor - Child");
		 C.m1();
	}
	Demo(int a){
		 System.out.println(a);
	}
	public static void main(String []args){

	/Demo obj=new Demo(10);
           Demo obj1=new Demo(new Parent());
	
	   Demo obj2=new Demo(new Child());
	   
	}


}

