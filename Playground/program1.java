class Demo{
	Demo(){
		System.out.println("In Constructor-Demo");
	}
}
class DemoChild extends Demo{
	DemoChild(){
		System.out.println("In Constructor-DemoChild");
	}
}
class Parent{
	Parent(){
		System.out.println("In Constructor-Parent");
	}
         Demo m1(){
		System.out.println("In M1 parent");
                  return new Demo();
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In Constructor-Child");
	}
     DemoChild m1(){
	       System.out.println("In M1-Child");
                 return new DemoChild();
       }

}

class Client{
	public static void main(String []args){
		Parent p=new Child();
		p.m1();
	
	}
}
