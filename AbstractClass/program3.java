//***************************************Real time eg- Abstract Class ************************************************************
 
abstract class India{
	India(){
		System.out.println("Parent Constructor-India");
	}
		void State(){
		System.out.println("Maharashtra");
	}
        abstract void city();
}


 class Child extends India{
	 Child(){
		 System.out.println("In Child Constructor");
	 }
		 void city(){
		   System.out.println("Pune , Mumbai , nashik ,nagpur,");
	 }
 
	 }
	 
class Client1{
	public static void main(String []args){
		Child obj=new Child();
		obj.State();
		obj.city();
	}
}
