//Abstract classes
//Abtract class cannot be instantiated - Object cannot be made of this class
//Abstract class is abstraction from 0%-100%
abstract class Parent{
	void Career(){
		System.out.println("Doctor");
	}
	abstract void Marry();                       //Abstract method
}
class Client{
	public static void main(String []args){
		Parent obj=new Parent(); 
	}
}
