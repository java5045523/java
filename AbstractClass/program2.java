//Code 2: abstract Class
abstract class Parent1{
	void career(){
		System.out.println("Doctor");
	}
       abstract void marry();          //if no body then declare method has Abstract
}

class Child1 extends Parent1{
	void marry(){
		  System.out.println("Alia Bhatt");
	}
}
class Client1{
	public static void main(String []args){
		Parent1 obj=new Child1();
		obj.career();
		obj.marry();
	}
}
//O/P: Doctor 
//     Alia bhatt
