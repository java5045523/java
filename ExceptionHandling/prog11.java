import java.util.*;
class OverSpeedException extends RuntimeException{
       OverSpeedException(String msg) {
                super(msg);
        }
}

class Client11{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int arr[]=new int[5];

		System.out.println("Enter the value");
		for (int i=0;i<arr.length;i++){
			int data =sc.nextInt();


			if(data>150)
				throw new OverSpeedException("Please maintain speed below 150Km/hr");

			arr[i]=data;
		}
	for(int i=0;i<arr.length;i++){
		System.out.println(arr[i]+ " ");
	}
	}
}
