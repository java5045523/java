//Null pointer Exception-Run time
class Demo4{
	  
	void m1(){
		  System.out.println("In m1");
	}
	void m2(){
		  System.out.println("In m2");
	}
	public static void main(String[] args){
		  System.out.println("Start Main");
		  Demo4 obj=new Demo4();
		  obj.m1();
		
	      	obj=null;  
	
	}
			  obj.m2();
		    System.out.println("End");
	}
}
//O/P: start main
//      in m1
//      Null pointer Exception
		  
