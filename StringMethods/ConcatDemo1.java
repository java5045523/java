//                                                               Method 1: "concat"
// Description : concatinate String to this String. Another string is concatinated with the first string.
// parameters : String 
// Return type : String

class ConcatDemo{
	public static void main(String []args){
		String str1="Core2";
		String str2= "Web";

		String str3= str1.concat(str2);     // Core2Web
 
		System.out.println(str3);           // Core2Web
	}
} 
//concate method is called from string class.
