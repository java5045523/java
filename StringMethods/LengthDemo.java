//                                   Method 2 : "Length" 
// public int length();
// Description : It returns the number of characters contained in the strings.
//parameters   : No parameter
//return type  : Integer
 
class LengthDemo1{
	public static void main(String []args){
		String str1="Core2Web";
               String str2 = "Karan Mahadik";
		System.out.println(str1.length());  //8
                 System.out.println(str2.length());  //12 also counts the space
	}
}
