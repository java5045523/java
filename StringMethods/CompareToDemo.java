//String - Method : compareTo (String str2);
//description     : it compares the str1 & str2 (case sensitive) , if both the strings are equal it returns 0 otherwise return the comparison.
//eg              : str1.compareTo(str2)
//Parameters      : String (second String)
//Return type     : Integer
 
class CompareToDemo{
	public static void main(String []args){
		String str1 = "Ashish";
		String str2 = "Ashish";


		System.out.println(str1.compareTo(str2));    // str1 And str2 are same content so it will return 0

	 String str3 = "ashish";          //asci value of a= 97
                String str4 = "Ashish";   //asci value of A=65
		  System.out.println(str3.compareTo(str4));  //  97-65= 32 Difference is the output. 
	}

}
