// Method      : public int CompareToIgnoreCase(String str1);
// Description :It compares str1 and str2 . it ignores upper case and lower case 
// Parameters  :String
// return type :integer     (if same content it will return : 0)

class CompareIgnoreDemo1{
	public static void main(String []args){

		String str1= "Shashi";   
		String str2= "shashi";

		System.out.println(str1.compareToIgnoreCase(str2));  // content is same if upper case and lower case ignored . it will return 0
	String str3= "shashi";               // a asci value is : 97
                String str4= "shysha";        // y values is : 121

	    System.out.println(str4.compareToIgnoreCase(str3));   // it returns asci value difference of : 121-97= 24
	}


}
