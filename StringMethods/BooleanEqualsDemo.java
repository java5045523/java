// MEthod : public boolean equals (Object anObject);
// descriptipn : Predicate which compares anObject to this. This is true only for Strings with the same character sequence .
// returns true if anObject is semantically equal to this.
// Parameters : Object(anObject)
// Return type : boolean

class EqualsDemo{
	public static void main(String []args){
		String str1= "Shashi";
		String str2= new String ("Shashi");

		System.out.println(str2.equals(str1));  //both the content are same with case sensitive .it will return true

  String str3= "Shashi";
                String str4= new String ("SHashi");

		        System.out.println(str3.equals(str4));  // it will return false. str4 has Capital H . 
	}
}
