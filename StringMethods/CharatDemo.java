// Method 3 : CharAt(int index);
// description : It returns the character located at specified index within the given String.
// Parameters: Integer(index)
// return type : Character
class CharAtDemo1{
	public static void main(String []args){
		 String str1= "Core2Web";
		 System.out.println(str1.charAt(2));    //r at index 2
		   System.out.println(str1.charAt(4));   //2
		     System.out.println(str1.charAt(5));   //w
		       System.out.println(str1.charAt(8));   // Error: StringIndexOutOfBoundsException
	}
}


