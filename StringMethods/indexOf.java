//Method : public int indexOf(char ch,int from index);
//Description : finds the first instance of the characters in given string.
//parameters: character (ch to find ) and  Integer (index to start the search from)
//return type : integer
class IndexOfDemo{
	public static void main(String []args){

		String str1="Narendra Modi";

		System.out.println(str1.indexOf('n',0));   //4
			  System.out.println(str1.indexOf('N',0));    //0				
		       System.out.println(str1.indexOf('f',0));	   //-1 if character not found it returns -1. 
	}
}
