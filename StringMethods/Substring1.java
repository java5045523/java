//Method       : public String subString(int index);
//Description  : creates a substring of the given string starting at a specified index and ending at the end of given string
//Paramrter    :    Integer (index of the string)
//Return type  : String

class SubstringDemo{
	public static void main(String []args){
		String str1= "Narendra Modi";

		System.out.println(str1.substring(2));  //rendra Modi // String started from 0    
				  System.out.println(str1.substring(0,6));  //Narend String starts at 0 and ends at 5.  //0,1,2,3,4,5
			   System.out.println(str1.substring(6));  // String started from 6 til end . //ra Modi
	 System.out.println(str1.substring(4,7)); //ndr   //4,5,6
	}

}
