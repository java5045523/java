// Method : public boolean equalsIgnoreCase ( String anotherString);
// Description: Compares a String to this String ignoring upper/lower case.
// Parameters: String (str2)
// return type: boolean

class EqualsDemo2{
	public static void main(String []args){
		String str1= "Shakti";
		String str2= "shakti";

		System.out.println(str1.equalsIgnoreCase(str2));    //if both content same ignoring case it returns true
	String str3= "Shakti";
                String str4= "bhakti";

                System.out.println(str3.equalsIgnoreCase(str4));       //false
	}

}
