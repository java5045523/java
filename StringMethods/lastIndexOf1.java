// Method      : public int lastIndexOf(char ch ,int from index)
// Description : finds the last instance of the character in the given string.
// Parameters  : character (ch to find ). Integer (index upto search)
// return type : Integer

class LastIndexOfDemo{
	public static void main(String []args){
		String str1= "Narendra Modi";

		System.out.println(str1.lastIndexOf('n',6));  //4
		System.out.println(str1.lastIndexOf('k',13));  //-1  if ch not found it returns -1 
		  System.out.println(str1.lastIndexOf('i',14));		//12			      
	}
}
