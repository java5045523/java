//Method       : public String replace(char old char,char new char);
//Description  :replace every instance of a characher in the given string with a new character .
//Paramrter    :      character
//Return type  : String 


class ReplaceDemo{
	public static void main(String []args){
		String str1= "Narendra Modi";

		System.out.println(str1.replace('d','b'));  //Narenbra Mobi
	    System.out.println(str1.replace('r','c'));     //Nacendca Modi
 System.out.println(str1.replace('k','c')); //k not found characher . it gives output of original string.
	} 							 
}
