//Program to create mystringlength method to count no of characters from strings.

class ToCharArray{                                 

static int myStrLen(String str2){
char arr[]= str2.toCharArray();                 // toCharArray : String is converted into array
int count =0;
for (int i=0 ;i<arr.length;i++){               
	count++;
}
		return count;
	}




public static void main(String []args){
		String str1= "ShashiBagal";
		System.out.println(str1.length());   //str1.length is predefined method   //11

             int len = (myStrLen(str1);			     // Calling user defined method myStrLen with argument str1
               System.out.println(len);      //11
		      
	}
}

//O/P : 11
//      11
