// Method :  public synchronized StringBuffer delete(int start ,int end);
// Parameter : Integer (start the 1st ch to delete)
//             Integer (end the index after the last ch to delete)  end number - 1;
// Return type: StringBuffer

class DeleteDemo{
	public static void main(String []args){
		StringBuffer bd= new StringBuffer("GameOfThrones");
		System.out.println(bd);  //GameOfthrones
		bd.delete(4,7);
		System.out.println(bd);  //Gamehrones
	}
}
