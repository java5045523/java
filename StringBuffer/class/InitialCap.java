// topic : Initial capacity declaring for the string 
class SBdemo1{
	public static void main(String []args){
		StringBuffer sb= new StringBuffer (100);   //Initial Capacity of string is declared 100 characters
		sb.append("Beincaps");
		sb.append("core2web");
		System.out.println(sb);          // Beincapscore2web
		System.out.println(sb.capacity());      //100
		sb.append("Incubator");
		System.out.println(sb);         //Beincapscore2webIncubator
                System.out.println(sb.capacity());     //100
	}

}
