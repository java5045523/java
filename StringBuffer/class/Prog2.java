// StringBuffer Append method explanation code. 
class SBdemo2{
	public static void main(String []args){
		String str1= "Shashi";          //scp //1000 
		String str2= new String ("Bagal");          // scp //2000

		StringBuffer str3= new StringBuffer ("Core2web");      //heap //3000

	      // String str4= str1.append(str3);   //Error:StringBuffer cannot be converted into String
                                                        //String class doesnt have append method
			
StringBuffer str4=str3.append(str2);                  //str3 and str4 has same address //append method accept string Parameter too.
String str5= str1.concat(str2);                    // when concat is done new object is created 
             
		System.out.println("str1:" + " " + str1);
                System.out.println("str2:" + " " + str2);
                System.out.println("str3:" + " " + str3);          //Core2webShashi
                System.out.println("str4:" + " " + str4);         //Core2WebShashi
 System.out.println("str5:" + " " + str5);                        //ShashiBagal

		System.out.println();

          System.out.println(System.identityHashCode(str1));           //1000
		System.out.println(System.identityHashCode(str2));     //2000
		System.out.println(System.identityHashCode(str3));     //4000
	 System.out.println(System.identityHashCode(str4));    	 //4000
	  System.out.println(System.identityHashCode(str5));        //5000
	}



}
