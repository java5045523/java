// program to check the capacity of StringBuffer
class StringBufferDemo6{
	public static void main(String []args){

		StringBuffer sb=new StringBuffer();       //empty StringBuffer string
	   System.out.println(sb.capacity());               //default capacity of StringBuffer is 16
          System.out.println(sb);                         // blank

	  sb.append("Shashi");                             //adding Shashi to empty string 
	  System.out.println(sb.capacity());                 //16
	  System.out.println(sb);                         //Shashi

	  sb.append("Bagal");                                //Adding bagal to string 
	  System.out.println(sb.capacity());                    //16
	  System.out.println(sb);                              //ShashiBagal

	  sb.append("Core2Web");                             //Adding Core2Web to string 
	  System.out.println(sb.capacity());                  // now string size was 16 but string characters are more then 16 
							    // so calcuted like this :
							    // Current capacity + 1 * 2 =  16 + 1 *2 = 34
							    // 34 is the current capacity
	  System.out.println(sb);                           // ShashiBagalCore2Web
 


	  sb.append("Karan25489632f15");            // total string has 35 characters more then 34 
						    //  now capacity will be calculated : 
						    //  34 + 1 * 2 =70
	  System.out.println(sb.capacity());            // 70
	  System.out.println(sb);

	}
}
