class StringBufferdemo1{
	public static void main(String []args){
		String str1= "Shashi";               //scp
		String str2= str1.concat("Bagal");     //heap

		System.out.println(str1);    //Shashi      //1000
		 System.out.println(str2);   //ShashiBagal   //2000 new object on heap
	}
}
