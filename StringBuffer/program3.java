// StringBuffer Concept
 
class StringBufferDemo3{
	public static void main(String []args){
		StringBuffer str1= new StringBuffer("Shashi");  // new object on heap wil be created
StringBuffer str2=new StringBuffer("");
		System.out.println(System.identityHashCode(str1));    //1000
		str1.append("Bagal");             // append is to concat with e.g ShashiBagal

		System.out.println(str1);                                 //ShashiBagal
		System.out.println(System.identityHashCode(str1));          //1000
	
		
	//To check string str1 Capacity
	System.out.println(str1.capacity()); //22
      System.out.println(str2.capacity());   //empty string default size/capacity is 16 charachers.	

	}


}

