
class StringDemo4{
        public static void main(String []args){
                String str1= "Akash";     //stored in stringconstantpool // 1000
                String str2= str1;        // reference to str1           // 1000
		String str3= new String (str2);                     //will be Stored on heap outside scp // 2000

		System.out.println(System.identityHashCode(str1));   // 1000 
		System.out.println(System.identityHashCode(str2));   // 1000 
		System.out.println(System.identityHashCode(str3));   // 2000
	}
}

