// Hashcode -  returns the same value (integeres) if two strings content is same.
class HashCodeDemo{
	public static void main(String []args){
		String str1= "Karan";                 //stored on :scp
		String str2=new String ("Shashi");    //stored on : heap
		String str3= "Shashi";                //stored on : scp

		String str4= new String ("Shashi");   // stored on :heap

		System.out.println(str1.hashCode());       //72485652
		 System.out.println(str2.hashCode());     //-1819698008   Shashi 
		  System.out.println(str3.hashCode());    //-1819698008   Shashi 
		   System.out.println(str4.hashCode());   // -1819698008  Shashi
	}
}

//str2 str3 str4 has same content named as "Shashi" so hashcode returned values are same.
