class StringDemo5{
	public static void main(String []args){
		String str1="Karan";          // stored on SCP      // 1000
		String str2= "Mahadik";        //stored on scp    // 2000

		System.out.println(str1 + str2);          //KaranMahadik       
 
		String str3= "KaranMahadik";         // stored on Scp       //3000
		String str4= str1 + str2 ;           // KaranMahadik         
		String str5= "KaranMahadik";         //reference to str3 // 3000				    
                  System.out.println(System.identityHashCode(str1));   //1000
                 System.out.println(System.identityHashCode(str2));      // 2000
		System.out.println(System.identityHashCode(str3));         // 3000
		System.out.println(System.identityHashCode(str4));	// 4000 new Object will be created outside scp in heap
	System.out.println(System.identityHashCode(str5));
	}

}
