class StringDemo6{
	public static void main(String []args){
		String str1= "Shashi";
		String str2= " Bagal ";
		System.out.println(str1);
		System.out.println(str2);

		str1.concat(str2);   // new object will be created in heap outside SCP.

		System.out.println(str1);
		System.out.println(str2);

	}
}

//output:  Shashi 
//         Bagal

//	 Shashi
//	 Bagal
