//accesing string location and address .
class StringDemo2{
        public static void main(String []args){
                String str1= "Core2Web";         // Stored on String constant pool

                String str2= new String ("Core2Web");   // Stored on Heap

                char str3[]={'C','2','W'}; //location : Heap // characters goes internally has integer
             
 		String str4= "Core2Web";
		  String str5= new String ("Core2Web");
	

  System.out.println(System.identityHashCode(str1));       //Unique ID: 1000
   System.out.println(System.identityHashCode(str2));      //Unique ID: 2000
    System.out.println(System.identityHashCode(str3));      //          3000

      System.out.println(System.identityHashCode(str4));                //1000
         System.out.println(System.identityHashCode(str5));             // 4000
	}

}
