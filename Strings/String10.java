class Sdemo{
	public static void main(String []args){
		String str1= "Karan Mahadik";               //scp
		String str2= "Karan" +" " + "Mahadik";       

		System.out.println(str1==str2);             //true   // == check IdentityHashCode
		System.out.println(System.identityHashCode(str1));
		 System.out.println(System.identityHashCode(str2));

		 String str3= "Karan";                     //scp new object
		 String str4= str3 + " " + "Mahadik";	 //because of concat new object will be created
		 System.out.println(str2 == str4);   //false

		 System.out.println(System.identityHashCode(str3));
		 System.out.println(System.identityHashCode(str4));  
	}
}
