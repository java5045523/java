// code to explain functioning of " + "operator  on strings  and also concat method.
class StringDemo7{
	public static void main(String []args){
		String str1= "Karan";
		String str2="Mahadik";

		String str3=str1 + str2;              //calls append method from StringBuilder class
		String str4=str1.concat(str2);       //calls concat method from String class

		 System.out.println(str3);       // KaranMahadik

		 System.out.println(str4);       // KaranMahadik

         System.out.println (System.identityHashCode(str1));
	 System.out.println (System.identityHashCode(str2));
	 System.out.println (System.identityHashCode(str3));
	 System.out.println (System.identityHashCode(str4));
         
System.out.println();
         
//hashcode checks the content of the strings if its same then address is same.
          System.out.println(str1.hashCode());   // 1000
	  System.out.println(str2.hashCode());   // 2000 
	  System.out.println(str3.hashCode());    //3000
	  System.out.println(str4.hashCode());    // 3000

	}
}

