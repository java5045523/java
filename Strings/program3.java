class StringDemo3{
	public static void main(String []args){
		String str1= "Akash";
		String str2=  "Akash";
		String str3= new String("Akash");
		String str4= new String ("Akash");
		String str5= new String ("Shahrukh");
		String str6= "Shahrukh";
		String str7= new String ("SAM");
		String str8= new String ("SAM");


		System.out.println(System.identityHashCode(str1));    // 1000
		System.out.println(System.identityHashCode(str2));   // 1000
		System.out.println(System.identityHashCode(str3));    // 2000
		System.out.println(System.identityHashCode(str4));     // 3000
		System.out.println(System.identityHashCode(str5));     // 4000
		System.out.println(System.identityHashCode(str6));      // 5000
              System.out.println(System.identityHashCode(str7));          // 6000
	System.out.println(System.identityHashCode(str8));              // 7000
	} 
}
