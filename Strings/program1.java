class StringDemo1{
	public static void main(String []args){
		String str1= "Core2Web";         // Stored on String constant pool
 
  		String str2= new String ("Core2Web");   // Stored on Heap

		char str3[]={'C','2','W'}; //location : Heap // characters goes internally has integer
		System.out.println(str1);
		 System.out.println(str2);
		  System.out.println(str3);


	}
}

//Output : 
//Core2Web
//Core2Web
//C2W


