//1.WAP to take input from user in 3D array and skip the diagonal elements while printing the array.
//Ex.     1. 2. 3. 
//        4.  5. 6.  
//        7.  8. 9. 
//Output- 2,3,4,6,7,8
import java.util.*;
class ArrayDemo{
	public static void main(String []args){
        Scanner sc= new Scanner(System.in);

	int arr[][]={{},{},{}};

	for (int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			System.out.println("Enter array elements");
			arr[i][j]=sc.nextInt();
		}
	}

	for(int i=1;i<=3;i++){
		for(int j=1;j<=3;j++){
			System.out.print(arr[i][j]+ " ");
			}
	}
	}
}
	
